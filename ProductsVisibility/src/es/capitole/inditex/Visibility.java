/**
 * 
 */
package es.capitole.inditex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import es.capitole.inditex.dto.Product;
import es.capitole.inditex.dto.Size;
import es.capitole.inditex.dto.Stock;

/**
 * @author Marcos
 *
 */
public class Visibility {

	// Estructuras para la lectura de los csvs
	static Set<Product> productList = new TreeSet<>();
	static List<Size> sizeList = new ArrayList<>();
	static List<Stock> stockList = new ArrayList<>();

	public static void main(String[] args) {

		try {
			// Leemos csvs
			readFiles();

			// Guardamos en este set todos los productos especiales
			var productosEspeciales = sizeList.stream().filter(size -> size.isSpecial()).map(a -> a.getProductId())
					.collect(Collectors.toSet());

			// Montamos una estructura Map<Product, List<Size>>
			var agrupacionProducto = sizeList.stream().collect(Collectors.groupingBy(Size::getProductId));

			// Eliminamos todas las Sizes que no estén en Stock o no sean BackSoon
			agrupacionProducto
					.replaceAll((k, v) -> v.stream().filter(estaEnStock.or(esBackSoon)).collect(Collectors.toList()));

			// ELiminamos los productos especiales que no cumplen casuística, los productos
			// especiales tienen que tener
			// al menos una size de cada grupo
			productosEspeciales.forEach(a -> {
				if (agrupacionProducto.get(a).stream().filter(c -> c.isSpecial()).count() == 0
						|| agrupacionProducto.get(a).stream().filter(c -> !c.isSpecial()).count() == 0) {
					agrupacionProducto.remove(a);
				}
			});

			// Eliminamos del map los productos cuya List<Size> sea vacía. 
			agrupacionProducto.entrySet().removeIf(entry -> entry.getValue().isEmpty());

			// Lista de productos que tenemos que mostrar al usuario, cruzamos con tabla
			// maestra de productos para ordenación y guardamos resultado ordenado en una Lista.
			var avalaiblesProducts = productList.stream()
					.filter(product -> agrupacionProducto.keySet().stream().anyMatch(p -> product.getId() == p))
					.collect(Collectors.toList());

			// Imprimimos por pantalla el resultado según el formato pedido.
			System.out.println(avalaiblesProducts.stream().map(product -> Long.toString(product.getId()))
					.collect(Collectors.joining(",")));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Problem reading files");
			e.printStackTrace();
		}

	}

	// Función que lee los csvs, elimina los saltos de línea y vuelca la
	// información a las estructuras de datos
	private static void readFiles() throws IOException {

		try (BufferedReader br = new BufferedReader(new FileReader(new File("resources/files/product.csv")))) {
			productList = br.lines().filter(s -> s.trim().length() > 0).map(mapToProduct)
					.collect(Collectors.toCollection(TreeSet::new));
		}

		try (BufferedReader br = new BufferedReader(new FileReader(new File("resources/files/size.csv")))) {
			sizeList = br.lines().filter(s -> s.trim().length() > 0).map(mapToSize).collect(Collectors.toList());
		}

		try (BufferedReader br = new BufferedReader(new FileReader(new File("resources/files/stock.csv")))) {
			stockList = br.lines().filter(s -> s.trim().length() > 0).map(mapToStock).collect(Collectors.toList());
		}

	}

	// Predicado para ver si la size BackSoon
	private static Predicate<Size> esBackSoon = (size) -> {

		return size.isBackSoon();
	};

	// Predicado para validar si la size está en stock
	private static Predicate<Size> estaEnStock = (size) -> {

		return stockList.stream().anyMatch(stock -> stock.getSizeId() == size.getId() && stock.getQuantity() > 0);
	};

	// Function para leer los product de cada una de las lineas del fichero
	private static Function<String, Product> mapToProduct = (line) -> {

		String[] p = line.split(",");
		Product item = new Product();
		item.setId(Long.parseLong(p[0].trim()));
		item.setSequence(Long.parseLong(p[1].trim()));
		return item;
	};

	// Function para leer los sizes de cada una de las lineas del fichero
	private static Function<String, Size> mapToSize = (line) -> {

		String[] p = line.split(",");
		Size item = new Size();
		item.setId(Long.parseLong(p[0].trim()));
		item.setProductId(Long.parseLong(p[1].trim()));
		item.setBackSoon(Boolean.parseBoolean(p[2].trim()));
		item.setSpecial(Boolean.parseBoolean(p[3].trim()));
		return item;
	};

	// Function para leer el stock de cada una de las lineas del fichero
	private static Function<String, Stock> mapToStock = (line) -> {

		String[] p = line.split(",");
		Stock item = new Stock();
		item.setSizeId(Long.parseLong(p[0].trim()));
		item.setQuantity(Long.parseLong(p[1].trim()));
		return item;
	};
}
