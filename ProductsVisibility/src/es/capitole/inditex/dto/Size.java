/**
 * 
 */
package es.capitole.inditex.dto;

/**
 * @author Marcos
 *
 */
public class Size {
	
	private long id;
	private long productId;
	private boolean backSoon;
	private boolean special;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public boolean isBackSoon() {
		return backSoon;
	}
	public void setBackSoon(boolean backSoon) {
		this.backSoon = backSoon;
	}
	public boolean isSpecial() {
		return special;
	}
	public void setSpecial(boolean special) {
		this.special = special;
	}
	@Override
	public String toString() {
		return "Size [id=" + id + ", productId=" + productId + ", backSoon=" + backSoon + ", special=" + special + "]";
	}

}
