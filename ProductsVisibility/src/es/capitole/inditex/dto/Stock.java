/**
 * 
 */
package es.capitole.inditex.dto;

/**
 * @author Marcos
 *
 */
public class Stock {
	
	private long sizeId;
	private long quantity;
	public long getSizeId() {
		return sizeId;
	}
	public void setSizeId(long sizeId) {
		this.sizeId = sizeId;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return "Stock [sizeId=" + sizeId + ", quantity=" + quantity + "]";
	}

}
