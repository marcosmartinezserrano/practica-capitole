/**
 * 
 */
package es.capitole.inditex.dto;

/**
 * @author Marcos
 *
 */
public class Product implements Comparable<Product>{
	
	private long id;
	private long sequence;
	
	
	public int compareTo(Product other) {
		
		if (this.sequence == other.getSequence()) {
            return 0;
        } else if (this.sequence > other.getSequence()) {
            return 1;
        } else {
            return -1;
        }
    }
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getSequence() {
		return sequence;
	}
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", sequence=" + sequence + "]";
	}

	

}
